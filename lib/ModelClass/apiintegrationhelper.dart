import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:touchworld2/ModelClass/Modelclass.dart';

import 'Modelclass.dart';

Future<List<Datum>> getDatumdetails() async {
  List<Datum> datums = [];
  var response = await http.post(Uri.parse("http://174.138.121.52:3005/v1/employee/getAllJobListOfFe"),
    body: {
      "executiveId": "42",
      "status": "incomplete",
      "job": "Pickup",
      "date": "2021-04-20"
    },
  ).catchError((error) {
    print(error);
  });
  if (response.statusCode == 200) {
    Map<String, dynamic> jsonBody = json.decode(response.body);
    datums = DatumResponse.fromJson(jsonBody).datums;
  }
  return datums;
}

