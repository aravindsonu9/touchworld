import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:touchworld2/BarCodeScanner.dart';
import 'package:touchworld2/JobRequestPage.dart';
import 'package:touchworld2/Maps.dart';
import 'package:touchworld2/ModelClass/Modelclass.dart';
import 'UploadPhotoPage.dart';

class home extends StatefulWidget {
  @override
  _homeState createState() => _homeState();
}

class _homeState extends State<home> {
  Future<Datum> list;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            children: [
              Spacer(),
              FlatButton(
                height: 50.0,
                minWidth: 400.0,
                color: Colors.blue,
                onPressed: () async {
                  //   Map<String,dynamic> payload = {"executiveId":"42","status":"incomplete","job": "Pickup",
                  //     "date": "2021-04-20"};
                  // list =  getDatumdetails("http://174.138.121.52:3005/v1/employee/getAllJobListOfFe",body: payload);
                  // print(list);
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => JobReq()));
                },
                child: Text("Job Requests"),
              ),
              SizedBox(
                height: 12.0,
              ),
              FlatButton(
                height: 50.0,
                minWidth: 400.0,
                color: Colors.blue,
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => BarCodePage()));
                },
                child: Text("Barcode Scanner"),
              ),
              SizedBox(
                height: 12.0,
              ),
              FlatButton(
                height: 50.0,
                minWidth: 400.0,
                color: Colors.blue,
                onPressed: () async {
                  WidgetsFlutterBinding.ensureInitialized();
                  final cameras = await availableCameras();
                  final firstcamera = cameras.first;

                  await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => UploadPhotoPage(
                                camera: firstcamera,
                              )));
                },
                child: Text("Upload Photo"),
              ),
              SizedBox(
                height: 12.0,
              ),
              FlatButton(
                height: 50.0,
                minWidth: 400.0,
                color: Colors.blue,
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Maps()));
                },
                child: Text("Address Picker"),
              ),
              Spacer()
            ],
          ),
        ),
      ),
    );
  }
}
