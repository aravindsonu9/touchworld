import 'package:flutter/material.dart';
import 'package:touchworld2/ModelClass/Modelclass.dart';
import 'package:touchworld2/ModelClass/apiintegrationhelper.dart';

class JobReq extends StatefulWidget {
  @override
  _JobReqState createState() => _JobReqState();
}

class _JobReqState extends State<JobReq> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 0, 11, 128),
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 0, 11, 128),
        title: Text("JOB REQUESTS"),
        brightness: Brightness.dark,
      ),
      body: FutureBuilder(
        future: getDatumdetails(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List<Datum> list = snapshot.data;
            if (list.isEmpty) {
              return Center(
                child: Text(
                  "No job request available",
                  style: TextStyle(color: Colors.white),
                ),
              );
            }
            return ListView.builder(
              itemCount: list.length,
              itemBuilder: (BuildContext context, int index) {
                Datum datum = list[index];
                return Padding(
                  padding: const EdgeInsets.only(
                    right: 20.0,
                    left: 20.0,
                    top: 10.0,
                  ),
                  child: Container(
                    height: 200.0,
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25.0),
                      ),
                      color: Color.fromARGB(255, 153, 240, 241),
                      child: Row(
                        children: [
                          Container(
                            padding: const EdgeInsets.all(18.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(height: 10.0),
                                Text("NAME :          ${datum.receiverName}"),
                                SizedBox(height: 10.0),
                                Text("ID :                  ${datum.id}"),
                                SizedBox(height: 10.0),
                                Text("AWB :             ${datum.aWB}"),
                                SizedBox(height: 10.0),
                                Text("ADDRESS :    ${datum.senderAddress}"),
                                SizedBox(height: 10.0),
                                Text("PHONE :        ${datum.senderMobile}"),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }
}
