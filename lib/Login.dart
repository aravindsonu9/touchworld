import 'package:flutter/material.dart';
import 'Home.dart';
import 'SQL/ModelClass.dart';
import 'SQL/SqlDbHelper.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}
class _MyHomePageState extends State<MyHomePage> {
  final usernameTextController = TextEditingController();
  final passwordTextController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body:Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
            children: <Widget>[
              Spacer(),
              TextField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(), labelText: "Username"),
                  maxLines: 1,
                  controller: usernameTextController,
                ),
              SizedBox(height: 15.0,),
              TextField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(), labelText: "Password"),
                  maxLines: 1,
                  controller: passwordTextController,
                ),
              SizedBox(
                height: 30.0,
              ),
              FlatButton(
                height: 50.0,
                minWidth: 400.0,
                color: Colors.blue,
                onPressed: () async {
                  DatabaseHelper.instance.insertTodo(Todo(
                      title: usernameTextController.text,
                      content: passwordTextController.text));
                  setState(() {});
                },
                child: Text("Register"),
              ),
              SizedBox(
                height: 6.0,
              ),
              FlatButton(
                height: 50.0,
                minWidth: 400.0,
                color: Colors.blue,
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>home()));
                },
                child: Text("Login"),
              ),
              // Container(
              //   height: 200.0,
              //   child: FutureBuilder<List<Todo>>(
              //     future: DatabaseHelper.instance.retrieveTodos(),
              //     builder: (context, snapshot) {
              //       if (snapshot.hasData) {
              //         return ListView.builder(
              //           itemCount: snapshot.data.length,
              //           itemBuilder: (BuildContext context, int index) {
              //             return ListTile(
              //               title: Text(snapshot.data[index].title),
              //               leading: Text(snapshot.data[index].id.toString()),
              //               subtitle: Text(snapshot.data[index].content),
              //             );
              //           },
              //         );
              //       } else if (snapshot.hasError) {
              //         return Text("Oops!");
              //       }
              //       return Center(child: CircularProgressIndicator());
              //     },
              //   ),
              // ),
              SizedBox(height: 30.0,),
              Spacer()
            ],
          ),
      ),
    );
  }
}
