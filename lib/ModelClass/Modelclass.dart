class DatumResponse {
  int status;
  List<Datum> datums;

  DatumResponse({this.status, this.datums});

  DatumResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['data'] != null) {
      datums = List.empty(growable: true);
      json['data'].forEach((v) {
        datums.add(new Datum.fromJson(v));
      });
    }
  }
}

class Datum {
  String assignedOnDateTime;
  String aWB;
  String bookingId;
  Map completedDateTime;
  String drsCollectionStatus;
  String executiveId;
  int id;
  String job;
  String jobDate;
  String materialCost;
  String materialCostCollected;
  String paymentMethod;
  String receiverAddress;
  String receiverMobile;
  String receiverName;
  String senderAddress;
  String senderMobile;
  String senderName;
  String serviceCharge;
  String serviceChargeCollected;
  Map startDateTime;
  String status;
  String reason;
  String remarks;
  Map totalCollected;

  Datum({
    this.id,
    this.bookingId,
    this.aWB,
    this.executiveId,
    this.job,
    this.status,
    this.assignedOnDateTime,
    this.jobDate,
    this.startDateTime,
    this.completedDateTime,
    this.serviceCharge,
    this.materialCost,
    this.serviceChargeCollected,
    this.materialCostCollected,
    this.totalCollected,
    this.drsCollectionStatus,
    this.reason,
    this.remarks,
    this.senderName,
    this.senderMobile,
    this.senderAddress,
    this.receiverName,
    this.receiverMobile,
    this.receiverAddress,
    this.paymentMethod,
  });

  Datum.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    bookingId = json['bookingId'];
    aWB = json['AWB'];
    executiveId = json['ExecutiveId'];
    job = json['job'];
    status = json['status'];
    assignedOnDateTime = json['assignedOnDateTime'];
    jobDate = json['jobDate'];
    startDateTime = json['startDateTime'];
    completedDateTime = json['completedDateTime'];
    serviceCharge = json['serviceCharge'];
    materialCost = json['MaterialCost'];
    serviceChargeCollected = json['serviceChargeCollected'];
    materialCostCollected = json['materialCostCollected'];
    totalCollected = json['totalCollected'];
    drsCollectionStatus = json['drsCollectionStatus'];
    reason = json['reason'];
    remarks = json['remarks'];
    senderName = json['senderName'];
    senderMobile = json['senderMobile'];
    senderAddress = json['senderAddress'];
    receiverName = json['receiverName'];
    receiverMobile = json['receiverMobile'];
    receiverAddress = json['receiverAddress'];
    paymentMethod = json['paymentMethod'];
  }

  // Datum.fromJSON(Map<String, dynamic> parsedJson) {
  //   this.assignedOnDateTime = parsedJson['assignedOnDateTime'];
  //   this.aWB = parsedJson['AWB'];
  //   this.bookingId = parsedJson['bookingId'];
  //   this.completedDateTime = parsedJson['completedDateTime'];
  //   this.drsCollectionStatus = parsedJson['drsCollectionStatus'];
  //   this.executiveId = parsedJson['ExecutiveId'];
  //   this.id = parsedJson['id'];
  //   this.job = parsedJson['job'];
  //   this.jobDate = parsedJson['jobDate'];
  //   this.materialCost = parsedJson['MaterialCost'];
  //   this.materialCostCollected = parsedJson['materialCostCollected'];
  //   this.paymentMethod = parsedJson['paymentMethod'];
  //   this.receiverAddress = parsedJson['receiverAddress'];
  //   this.receiverMobile = parsedJson['receiverMobile'];
  //   this.receiverName = parsedJson['receiverName'];
  //   this.senderAddress = parsedJson['senderAddress'];
  //   this.senderMobile = parsedJson['senderMobile'];
  //   this.senderName = parsedJson['senderName'];
  //   this.serviceCharge = parsedJson['serviceCharge'];
  //   this.serviceChargeCollected = parsedJson['serviceChargeCollected'];
  //   this.startDateTime = parsedJson['startDateTime'];
  //   this.status = parsedJson['status'];
  //   this.totalCollected = parsedJson['totalCollected'];
  // }
}
